import { SMS } from 'yunpian-sdk';

export function sendSmsAlert(deviceId) {
  const sms = new SMS({
    apikey: 'c511ebd1565da2da13fc5d428012aa46 '
  });

  async function send() {
    return await sms.singleSend({
      mobile: '18114919892',
      text: '【菲尔兹网络】您的水泵"' + deviceId + '"工作异常，水位过高，请及时采取措施。'
    });
  };

  return send().then(res => {
    console.log(res);
    return {
      code: res.code,
      errors: res.msg
    };
  });
}
