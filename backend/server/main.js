import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base'
// 只有这样才能让Meteor将这些脚本加入build bundle, 从而可以在meteor shell中调用
// 参见: https://github.com/meteor/meteor/issues/7629#issuecomment-239196322
if (false) {  // eslint-disable-line
  require('/imports/sample');
}
Meteor.publish('user', function () {
  return Meteor.users.find({_id: this.userId});
});
Meteor.startup(() => {
  const theOnlyUser = Meteor.users.find().fetch();

  if (theOnlyUser.length == 0) {
	var userlist = [
      {name:"admin",password:"pass",roles:['admin']},
      {name:"clerk",password:"clerk",roles:['clerk']},
      {name:"customer",password:"customer",roles:['customer']},
      {name:"checker",password:"checker",roles:['checker']},
	  
    ];
    _.each(userlist, function(userData) {
      console.log('Creating users: ');

      console.log(userData);

      id = Accounts.createUser({
        username : userData.name,
        password: userData.password,
        profile: { name: userData.name }
      });
      Roles.addUsersToRoles(id, userData.roles);
    })

  }
});
