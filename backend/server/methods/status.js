import Status from '/server/collections/status';
import { sendSmsAlert } from '/server/3rd-party/sms';

Meteor.methods({
  'status.sendSmsAlert': async (deviceId) => {
    const res = await sendSmsAlert(deviceId);
    return res;
  },
  'status.getByUser': (userName) => {
    const deviceIds = Meteor.call('devices.getByUser', userName);
    return Status.find({
      _id: {
        $in: deviceIds
      }
    }).fetch();
  },
  'status.get': (deviceId) => {
	  console.log(deviceId);
	  console.log(Status.findOne({
      _id: deviceId
    }));
    return Status.findOne({
      _id: deviceId
    });
  },
  'status.set': (deviceId, status, value) => {
    let retVal;

    if (deviceId) {
      if (status === 'flooding' && value === '异常') {
        const statusObj = Status.findOne({ _id: deviceId });
        if (!statusObj || statusObj.flooding[0].value !== '异常') {
          const res = Meteor.call('status.sendSmsAlert', deviceId);
          retVal = res.errors;
        }
      }

      Status.upsert({
        _id: deviceId,
      }, {
        $push: {
          [status]: {
            $each: [{
              ts: new Date() / 1,
              value
            }],
            $position: 0
          }
        }
      });
    }

    return retVal || (deviceId && status && value);
  },
  'status.toggle': (deviceId, status) => {
    const statusObj = Status.findOne({
      _id: deviceId
    });

    return Meteor.call('status.set', deviceId, status,
                       statusObj[status][0].value === '异常' ? '正常' : '异常');
  },
});
