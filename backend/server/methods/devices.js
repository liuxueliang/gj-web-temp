import Devices from '/server/collections/devices';

Meteor.methods({
  'devices.getByUser': (userName) => {
    return Devices.find({ 'userName': userName}).fetch().map(item => item.deviceId);
  },
});
