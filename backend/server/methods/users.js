import Users from '../collections/user'
Meteor.methods({
  'users.role': (userName) => {
    return Users.findOne({ 'username': userName},{"roles":1}).roles[0];
  },
});
