const Task = new Mongo.Collection('task');
Task.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
export default Task;