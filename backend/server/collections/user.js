const Users = Meteor.users;
Users.deny({
  update() { return true; }
});
export default Users;
