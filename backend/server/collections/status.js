const Status = new Mongo.Collection('status');

Status.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

export default Status;
