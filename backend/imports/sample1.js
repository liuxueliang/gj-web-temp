// 这个模块负责往数据库中添加较为完整的初始数据便于开发阶段使用
// 数据由我们自己制作, 因此比较完善
// 使用方法:
// 1. meteor reset
// 2. meteor shell
// 3. 在shell中执行如下代码:
//    require('/imports/sample').default();
//
import fs from 'fs';
import os from 'os';
import path from 'path';
import Devices from '/server/collections/devices';
import Status from '/server/collections/status';
import { getRandomInt, round, round2 } from '/server/helpers/math';

var createDummyDevices = function() {
  Devices.insert({
    userName: '科航',
    deviceId: '地下室'
  });
  Devices.insert({
    userName: '科航',
    deviceId: '花园'
  });
  Devices.insert({
    userName: '张三',
    deviceId: '浴室'
  });
};

var createDummyStatus = function() {
  const deviceIds = ['地下室', '花园', '浴室'];
  const nowDate = new Date() / 1;
  for (const deviceId of deviceIds) {
    const pump = [];
    const flooding = [];
    const startDate = getRandomInt(new Date(2016, 10), new Date(2017, 1));

    // 在时间段内水泵工作2-5次, 每次持续工作1-72小时
    const periodSecs = (nowDate - startDate) / 1000;
    const times = getRandomInt(2, 5);
    const durationSecs = [];
    const relativeIntervals = [];
    let totalInterval = 0;
    // 整个时间段包括`times`个水泵工作周期和`times+1`个间隔, 每个周期长和间隔长都随机生成
    for (let i = 0; i < times; i++) {
      const duration = getRandomInt(3600, 3600 * 72);
      durationSecs.push(duration);
      periodSecs -= duration;

      const rand = getRandomInt(100, 1000);
      relativeIntervals.push(rand);
      totalInterval += rand;
    }
    const rand = getRandomInt(100, 1000);
    relativeIntervals.push(rand);
    totalInterval += rand;

    const pumpStarts = [];
    const pumpEnds = [];
    let baseTime = startDate;
    for (let i = 0; i < times; i++) {
      const pumpStart = baseTime + periodSecs * 1000 * relativeIntervals[i] / totalInterval;
      const pumpEnd = pumpStart + durationSecs[i] * 1000;
      pumpStarts.push(pumpStart);
      pumpEnds.push(pumpEnd);
      baseTime = pumpEnd;
    }

    pump.unshift({
      ts: startDate,
      value: 'OFF'
    });
    flooding.unshift({
      ts: startDate,
      value: '正常'
    });
    for (let i = 0; i < times; i++) {
      pump.unshift({
        ts: pumpStarts[i],
        value: 'ON'
      });
      pump.unshift({
        ts: pumpEnds[i],
        value: 'OFF'
      });

      // 每个水泵工作周期有一半概率出现淹水, 一半概率不出现.
      if (getRandomInt(1, 2) === 1) {
        // 出现淹水, 淹水开始时间范围是水泵开始工作时间之前12小时到水泵停止工作前30分钟, 淹水
        // 结束时间是淹水开始时间后一分钟到水泵停止工作时间之后3小时.
        const floodingStart = getRandomInt(pumpStarts[i] - 12 * 3600 * 1000,
                                           pumpEnds[i] - 30 * 60 * 1000);
        flooding.unshift({
          ts: floodingStart,
          value: '异常'
        });
        flooding.unshift({
          ts: getRandomInt(floodingStart + 60 * 1000, pumpEnds[i] + 3 * 3600 * 1000),
          value: '正常'
        });
      }
    }

    Status.insert({
      _id: deviceId,
      pump,
      flooding,
    });
  }
};

export default function addDummyData() {
  // insert dummy content only if the db is empty
  if (!Devices.find().count() && !Status.find().count()) {
    createDummyDevices();
    createDummyStatus();
  }
}
