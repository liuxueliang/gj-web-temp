import acceptSaga from './Apply/acceptSaga';
import PickSaga from './Pick/PickSaga';
export default [
    acceptSaga,
    PickSaga
];