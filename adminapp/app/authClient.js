import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_CHECK, AUTH_ERROR, AUTH_GETROLE } from 'admin-on-rest';
import { createClass } from 'asteroid';
import { setLoggedUser, setLoggedUserRole } from 'admin-on-rest'
const Asteroid = createClass();
// Connect to a Meteor backend
const asteroid = new Asteroid({
  endpoint: 'ws://localhost:9000/websocket',
});
asteroid.ddp.on('added', (doc) => {
  if (doc.collection === 'users') {
    //console.log(doc);
    //asteroid.call('users.role','clerk').then((result => console.log(result)));
    if(doc.fields.roles == 'admin'){
      console.log('ok admin');
    }
  }
});
export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const { username,password } = params;
        
        return asteroid.loginWithPassword({username:username, password:password}).then( () => localStorage.setItem('username', username));
    }
    if(type === AUTH_GETROLE ){
        return asteroid.call('users.role',params);
    }
    if (type === AUTH_LOGOUT) {
        localStorage.removeItem('username');
        return Promise.resolve();
    }
    if (type === AUTH_ERROR) {
        return Promise.resolve();
    }
    if (type === AUTH_CHECK) {
        return localStorage.getItem('username') ? Promise.resolve(localStorage.getItem('username')) : Promise.reject();
    }
    return Promise.reject('Unkown method');
};
