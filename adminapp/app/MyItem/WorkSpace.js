import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    BooleanField,
    BooleanInput,
    CheckboxGroupInput,
    Create,
    Datagrid,
    DateField,
    DateInput,
    DisabledInput,
    Edit,
    EditButton,
    Filter,
    FormTab,
    ImageField,
    ImageInput,
    List,
    LongTextInput,
    NumberField,
    NumberInput,
    ReferenceManyField,
    Responsive,
    RichTextField,
    SelectInput,
    Show,
    ShowButton,
    SimpleForm,
    SimpleList,
    SimpleShowLayout,
    TabbedForm,
    TextField,
    TextInput,
    minValue,
    number,
    required,
    translate,
    Labeled,
} from 'admin-on-rest';
import SelectField from 'material-ui/SelectField'
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom';
import { Field, option, formValueSelector } from 'redux-form';
import { reduxForm } from 'redux-form';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox'
import {productType, productMap} from './TypeDefine'
const renderSelectField = ({ input, label, meta: { touched, error }, children,disabled, ...custom }) => (
  <SelectField
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    onChange={(event, index, value) => input.onChange(value)}
    children={children}
    style={{width: 300}}
    disabled={disabled}
    {...custom}/>
);
const renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
    <TextField
        hintText={label}
        floatingLabelText={label}
        errorText={touched && error}
        {...input}
        {...custom}
    />
);
const renderItem = (item) =>{ return ( <MenuItem value={item}  key={item} primaryText={item} />)};
export class ProductType extends Component {

    render() {
        const { record } = this.props;
        //console.log(this);
        if(record.finished === false && record.status === '待审核' ){
            return (    
              <Field name="Product" component={renderSelectField} >
              {productType.map(renderItem)}
              </Field>
            
            );
        }
        else{
            return (    
              <Field name="Product" disabled={true} component={renderSelectField} >
              {productType.map(renderItem)}
              </Field>
            );
        }

    }
}
export class TestCriteria extends Component {

    render() {
        const { record } = this.props;
        //console.log(this.props.productTypeVal);
        if(record.finished === false && record.status === '待审核' ){
            return (    
              <Field name="TestCriteria" component={renderSelectField} >
              {productMap[this.props.productTypeVal].map((item) =>{return( <MenuItem value={item[0]}  key={item[0]} primaryText={item[0]} /> )})}
              </Field>
            
            );
        }
        else{
            return (    
              <Field name="TestCriteria" disabled={true} component={renderSelectField} >
              {productMap[this.props.productTypeVal].map((item) =>{return( <MenuItem value={item[0]}  key={item[0]} primaryText={item[0]} /> )})}
              </Field>
            );
        }

    }
}
const selector = formValueSelector('record-form');
TestCriteria = connect(
  state => {
    const productTypeVal = selector(state, 'Product');
    return {productTypeVal};
  }
  )(TestCriteria)

 
export class MultiBox extends Component{
    handleCheck = (event, isChecked) => {
        const { input: { value, onChange } } = this.props;
        //console.log(event);
        if (isChecked) {
            onChange([...value, ...[event.target.value]]);
        } else {
            onChange(value.filter(v => (v != event.target.value)));
        }
    };
    test = {id:1, Dtype:'烟密度等级:GB8624-2012'};
    renderCheckbox = ({ input, label }) => {
        const {
        input: { value },
        optionText,
        optionValue,
        options,
        } = this.props;
        //console.log(this.props);
        return(
        <Checkbox
        key={this.test['id']}
        label={label}
        checked={value ? value.find(v => (v == this.test['id'])) !== undefined : false}
        onCheck={this.handleCheck}
        value={this.test['id']}
        />
        )
    }
    render(){
        const { choices, label, resource, source, record, productTypeVal, TestCriteriaVal, optionValue, value } = this.props;
        console.log(this.props);
        return(
        <div>
             <Field name="InsItems" component={this.renderCheckbox} label="Employed"/>
        </div>
        )
    }
};

MultiBox = connect(
  state => {
    const productTypeVal = selector(state, 'Product');
    const testCriteriaVal = selector(state,'TestCriteria');
    return {productTypeVal, testCriteriaVal};
  }
  )(MultiBox)
MultiBox.defaultProps = {
    addField: true,
    choices: [],
    options: {},
    optionText: 'name',
    optionValue: 'id',
};