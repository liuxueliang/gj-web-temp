import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom';
class DoButton extends Component {
	dealText = (record) =>{
	switch(record.status){
		case "审核拒绝":
			return "查看";
		case "待审核":
			return "审核";
		case "已付款":
			return "添加物流信息";
		case "审核未通过":
			return "添加物流信息";
		case "检测完成":
			return "添加物流信息";
		default:
			return "查看";		
	}
	};
    render() {
    	const { record } = this.props;
    	const Text = this.dealText(record);
    	if(record.finished === false){
	        return (
	            <span>
	                <RaisedButton containerElement={ <Link to={ `/MyItem/${record.id}`} /> }  primary={true} label={Text}/>
	            </span>
	        );
    	}
    	else{
	        return (
	            <span>
	               订单完成
	            </span>
	        );
    	}
    }
}
export default connect(null, null)(DoButton);