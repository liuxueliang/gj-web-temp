import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'material-ui/Checkbox';
import { connect } from 'react-redux';
import Labeled from 'admin-on-rest';
import {productType, productMap, choices} from './TypeDefine';
import { Field, option, formValueSelector } from 'redux-form';
export class CheckboxGr extends Component {
    handleCheck = (event, isChecked) => {
        const { input: { value, onChange } } = this.props;

        if (isChecked) {
            onChange([...value, ...[event.target.value]]);
        } else {
            onChange(value.filter(v => (v != event.target.value)));
        }
    };
    renderCheckbox = (choice) => {
        //console.log(this.props.value);
        const {
            input: { value },
            optionText,
            optionValue,
            options,
        } = this.props;
        //console.log(this.props.value);
        const disable = this.props.record.finished || (this.props.record.status != '待审核');
        const choiceName = React.isValidElement(optionText) ? // eslint-disable-line no-nested-ternary
            React.cloneElement(optionText, { record: choice }) :
            (typeof optionText === 'function' ?
                optionText(choice) :
                choice[optionText]
            );
        return (
            <Checkbox
                key={choice[optionValue]}
                checked={value ? value.find(v => (v == choice[optionValue])) !== undefined : false}
                onCheck={this.handleCheck}
                value={choice[optionValue]}
                label={choiceName}
                disabled={disable}
                {...options}
            />
        );
    }

    render() {
        const { choices, label, resource, source, record, productTypeVal, testCriteriaVal, optionValue, value } = this.props;
        const choice = productMap[productTypeVal].find((i)=>(i[0]==testCriteriaVal));
        //console.log(choice);
        if(choice){
            const filterchoices = choices.filter((i)=>( choice[1].includes(i.id) ));
            //console.log(filterchoices);
            return (
            
                <div>
                    {filterchoices.map(this.renderCheckbox)}
                </div>
            
            );
        }
        
        return (
            
                <div>
                    
                </div>
            
        );
    }
}
const selector = formValueSelector('record-form');
CheckboxGr = connect(
  state => {
    const productTypeVal = selector(state, 'Product');
    const testCriteriaVal = selector(state,'TestCriteria');
    return {productTypeVal, testCriteriaVal};
  }
  )(CheckboxGr)
CheckboxGr.defaultProps = {
    addField: true,
    choices: [],
    options: {},
    optionText: 'name',
    optionValue: 'id',
};

