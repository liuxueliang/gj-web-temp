import { UPDATE } from 'admin-on-rest';
export const ACCEPTAPPLY_LOADING = 'ACCEPTAPPLY_LOADING';
export const ACCEPTAPPLY_FAILURE = 'ACCEPTAPPLY_FAILURE';
export const ACCEPTAPPLY_SUCCESS = 'ACCEPTAPPLY_SUCCESS';
export const ACCEPTAPPLY = 'ACCEPTAPPLY';
export const AcceptApply = (id, data, basePath) => ({
    type: ACCEPTAPPLY,
    payload: { id, data: { ...data, ispicked: true }, basePath },
    meta: { resource: 'ApplyItem', fetch: UPDATE, cancelPrevious: false },
});