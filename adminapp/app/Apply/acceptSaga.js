import { put, takeEvery } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { showNotification } from 'admin-on-rest';
import { ACCEPTAPPLY, ACCEPTAPPLY_LOADING, ACCEPTAPPLY_FAILURE, ACCEPTAPPLY_SUCCESS } from './acceptAction';
export default function* acceptSaga() {
    yield [
        takeEvery(ACCEPTAPPLY_SUCCESS, function* () {
            yield put(showNotification('领取成功'));
            yield put(push('/'));
        })
    ];
}
