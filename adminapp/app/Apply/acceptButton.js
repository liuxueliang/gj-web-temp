import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import {AcceptApply as AcceptApplyAction } from './acceptAction'
class AcceptButton extends Component {
    handleAccept = () => {
        const { AcceptApply, record } = this.props;
        AcceptApply(record.id, record);
    }
    render() {
    	const { record } = this.props;
    	//console.log(record);
        return (
            <span>
                <RaisedButton onClick={this.handleAccept} primary={true} label="领取"/>
            </span>
        );
    }
}
AcceptButton.propTypes = {
    AcceptApply: PropTypes.func,
};
export default connect(null, {
	AcceptApply:AcceptApplyAction
})(AcceptButton);